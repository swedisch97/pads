﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Z_Func
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader file = new StreamReader("input.txt");
            StreamWriter output = new StreamWriter("output.txt");
            string s;

            while (!file.EndOfStream)
            {
                s = file.ReadLine();
                int[] a = BuildZ(s);
                for (int i = 1; i < a.Length; i++)
                {
                    output.Write(a[i] + " ");
                }              
            }
            output.Close();
        }

        static int[] BuildZ(string sT)
        {
            char[] s = sT.ToCharArray();
            
            int[] z = new int[sT.Length];
            int L = 0;
            int R = 0;
            int n = sT.Length;
            int j;
            for(int i = 1; i < n; i++)
            {
                if (i >= R)
                {
                    j = 0;
                    while (i + j < n && s[i + j] == s[j])
                    {
                        j++;
                    }
                    L = i;
                    R = i + j;
                    z[i] = j;
                }
                else
                {
                    if (z[i - L] < R - i)
                    {
                        z[i] = z[i - L];
                    }
                    else
                    {
                        j = R - i;
                        while (i + j < n && s[i + j] == s[j])
                        {
                            j++;
                        }
                        L = i;
                        R = i + j;
                        z[i] = j;
                    }
                }
                
            }
            return z;
        }
    }
}
